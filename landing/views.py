from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from .forms import statusForm
from .models import Status

# Create your views here.

def index(request):
    form = statusForm

    response = {'form' : form, 'all' : Status.objects.all()}
    return render(request, 'index.html', response)

response = {}
def add(request):
	if request.method == 'POST':
		form = statusForm(request.POST or None)
		if form.is_valid():
			response['status'] = request.POST['status']
			status = Status(status=response['status'])

			status.save()
			return render(request, 'add.html')

	else:
		return HttpResponseRedirect('')

def delete(request):
	form= Status.objects.all()
	form.delete()
	return render(request, 'delete.html')

def profile(request):
	return render(request, 'profile.html')

