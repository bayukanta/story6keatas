from .models import Status
from django import forms

class statusForm(forms.Form):
    status = forms.CharField(widget=forms.TextInput({'class' : 'form-control'}))