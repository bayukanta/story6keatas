from re import search

from django.http import HttpRequest
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from . import views
from .models import Status


# Create your tests here.



class ProfUnitTest(TestCase):
	def test_url_home_exist(self):
		response = Client().get('')
		self.assertEqual(response.status_code, 200)

	def test_url_add_exist(self):
		response = Client().get('/add')
		self.assertEqual(response.status_code, 302)

	def test_url_delete_exist(self):
		response = Client().get('/delete?')
		self.assertEqual(response.status_code, 200)

	def test_url_profile_exist(self):
		response = Client().get('/profile')
		self.assertEqual(response.status_code, 200)
		
	def test_using_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, views.index)

	def test_using_add_func(self):
		found = resolve('/add')
		self.assertEqual(found.func, views.add)

	def test_using_delete_func(self):
		found = resolve('/delete')
		self.assertEqual(found.func, views.delete)

	def test_using_profile(self):
		found = resolve('/profile')
		self.assertEqual(found.func, views.profile)

	def test_landing_html(self):
		response = Client().get('')
		self.assertTemplateUsed(response, 'index.html')

	def test_profile_html(self):
		response = Client().get('/profile')
		self.assertTemplateUsed(response, 'profile.html')

	def test_name_is_changed(self):
		request = HttpRequest()
		response = views.index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<title>Bay</title>', html_response)
		self.assertIn('<h1 class="text-center">Hello, Apa kabar?</h1>', html_response)

	def test_model_can_create_new_activity(self):
		new_activity = Status.objects.create(status="Test sebuah status")

		all_activities = Status.objects.all().count()
		self.assertEqual(all_activities, 1)

	def test_form_exist(self):
		request = HttpRequest()
		response = views.index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<form id="form" method="POST" action="/add">', html_response)

	def test_submit_button_exist(self):
		request = HttpRequest()
		response = views.index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<button class="btn btn-default" type="submit">', html_response)

	def test_deleteform_exist(self):
		request = HttpRequest()
		response = views.index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<form id="form1" method="DELETE" action="/delete">', html_response)	

	def test_delete_button_exist(self):
		request = HttpRequest()
		response = views.index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<button class="btn btn-default" type="submit">', html_response)	
